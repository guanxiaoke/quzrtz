package com.quartz.annotation;

import com.quartz.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * 自定义数据源注解切换
 *
 * 优先级：先方法，后类，如果方法覆盖了类上的数据源类型，以方法的为准，否则以类上的为准
 *
 * @author hanyun
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {

    /**
     * 切换数据源的名称
     */
    public DataSourceType value() default DataSourceType.MASTER;
}
