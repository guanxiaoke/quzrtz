package com.quartz.domain;

import java.io.Serializable;

/**
 * 定时任务调度表 sys_job
 *
 * @author hanyun
 */
public class SysJob extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //任务ID
    private Long jobId;

    //任务名称
    private String jobName;

    //任务组名
    private String jobGroup;

    //调用目标字符串
    private String invokeTarget;

    //corn执行表达式
    private String cronExpression;

    //计划执行错误策略（1立即执行 2执行一次 3放弃执行）
    private String misfirePolicy = "0";

    //是否并发执行（0允许 1禁止）
    private String concurrent;

    //状态（0 正常 1暂停）
    private String status;

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getInvokeTarget() {
        return invokeTarget;
    }

    public void setInvokeTarget(String invokeTarget) {
        this.invokeTarget = invokeTarget;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getMisfirePolicy() {
        return misfirePolicy;
    }

    public void setMisfirePolicy(String misfirePolicy) {
        this.misfirePolicy = misfirePolicy;
    }

    public String getConcurrent() {
        return concurrent;
    }

    public void setConcurrent(String concurrent) {
        this.concurrent = concurrent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
