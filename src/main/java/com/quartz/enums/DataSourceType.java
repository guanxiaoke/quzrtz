package com.quartz.enums;

/**
 * 数据源
 *
 * @author hanyun
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库1
     */
    SLAVE1,
    /**
     * 从库2
     */
    SLAVE2
}