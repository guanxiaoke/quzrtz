package com.quartz.task;

import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 *
 * @author hanyun
 */
@Component("tTask")
public class TestTask {
    public void tMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(s);
        System.out.println(b);
        System.out.println(l);
        System.out.println(d);
        System.out.println(i);
    }

    public void tParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void tNoParams()
    {
        System.out.println("执行无参方法");
    }
}